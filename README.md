# Sermon Buddy - Sermon Notes Application

[![Build Status](https://travis-ci.org/kevgathuku/sermonbuddy.svg)](https://travis-ci.org/kevgathuku/sermonbuddy)

Sermon Buddy is a Django Application to help in 
managing sermon notes.

It provides a one-stop convenient place to store and 
keep track of your sermon notes.