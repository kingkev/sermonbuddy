from .base import FunctionalTest
from unittest import skip
from selenium import webdriver


class NewVisitorTest(FunctionalTest): 

    def test_can_create_new_note_and_retrieve_it_later(self):
        #Kevin has just developed a new sermon notes app. 
        #He invites his friend Coco to check it out
        self.browser.get(self.server_url)

        #She notices the page title and header mention sermon notes
        self.assertIn('Sermon', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Sermon', header_text)

        #She notices a button asking her to Add a new Sermon" and clicks it
        self.browser.find_element_by_id('new_sermon').click()

        # When she clicks on it, she is taken to a page whose header contains
        #"Add a New Sermon"
        new_header = self.browser.find_element_by_tag_name('h1').text
        self.assertEqual("Add a New Sermon", new_header)

        #On this new page, she is asked to enter the additional details regarding the sermon
        #For example, speaker, and the actual notes.
        title = self.browser.find_element_by_id('id_sermon_title')
        self.assertEqual(
                title.get_attribute('placeholder'),
                'Enter a Sermon Title'
        )

        speaker = self.browser.find_element_by_id('id_sermon_speaker')
        self.assertEqual(
                speaker.get_attribute('placeholder'),
                'The Speaker'
        )

        sermon_notes = self.browser.find_element_by_id('id_sermon_notes')
        self.assertEqual(
                sermon_notes.get_attribute('placeholder'),
                'Write your Notes Here'
        )

        sermon_submit = self.browser.find_element_by_id('id_sermon_submit')

        title.send_keys("Roses to Dishes")
        speaker.send_keys("Pastor M")
        sermon_notes.send_keys("A Sermon about marriage")
        sermon_submit.submit()

        # When she presses the save button, she is taken to a page whose URL is in the format '/notes/'
        # On this page, she can view all the details she entered
        coco_notes_url = self.browser.current_url
        self.assertRegex(coco_notes_url, '/notes/.+')

        self.check_for_row_in_list_table('Roses to Dishes')
        self.check_for_row_in_list_table('Pastor M')
        self.check_for_row_in_list_table('A Sermon about marriage')

        # Now a new user, Francis, comes along to the site.

        ## We use a new browser session to make sure that no information
        ## of Coco's is coming through from cookies etc
        self.browser.quit()
        self.browser = webdriver.PhantomJS()

        # Francis visits the home page.  There is no sign of Coco's
        # sermon notes
        self.browser.get(self.server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Roses to Dishes', page_text)
        self.assertNotIn('A Sermon about marriage', page_text)

        # Francis writes his own sermon notes.
        self.browser.find_element_by_id('new_sermon').click()

        input_title = self.browser.find_element_by_id('id_sermon_title')
        input_speaker = self.browser.find_element_by_id('id_sermon_speaker')
        input_notes = self.browser.find_element_by_id('id_sermon_notes')

        input_title.send_keys('Africa Rising')
        input_speaker.send_keys('Pastor Oscar')
        input_notes.send_keys('A sermon about Evangelism')

        input_notes.submit() #Submits the form in which this element is a part of

        # Francis gets his own unique URL
        francis_notes_url = self.browser.current_url
        self.assertRegex(francis_notes_url, '/notes/.+')
        self.assertNotEqual(francis_notes_url, coco_notes_url)

        # Again, there is no trace of Coco's notes
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Roses to Dishes', page_text)
        self.assertNotIn('A Sermon about marriage', page_text)
        self.assertIn('Africa Rising', page_text)

        # Satisfied, they both go back to sleep
