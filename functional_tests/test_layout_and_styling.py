from .base import FunctionalTest


class LayoutAndStylingTest(FunctionalTest):

    def test_layout_and_styling(self):
        # Coco goes to the home page
        self.browser.get(self.server_url)
        self.browser.set_window_size(1024, 768)

        #She navigates to the new sermon note page
        self.browser.find_element_by_id('new_sermon').click()

        # She notices the notes input element is nicely centered
        sermon_notes_input = self.browser.find_element_by_id('id_sermon_notes')
        self.assertAlmostEqual(
            sermon_notes_input.location['x'] + sermon_notes_input.size['width'] / 2,
            512,
            delta=5
        )
