from .base import FunctionalTest


class ItemValidationTest(FunctionalTest):
    
    def test_cannot_add_empty_sermon_title(self):
        # Coco goes to the add sermon page and accidentally tries to submit
        # an empty sermon title. She hits Enter on the empty input box
        self.browser.get(self.server_url)
        self.browser.find_element_by_id('new_sermon').click()
        self.browser.find_element_by_id('id_sermon_title').send_keys('\n')

        # The page refreshes, and there is an error message saying
        # that sermon titles cannot be blank
        error = self.browser.find_element_by_css_selector('.has-error')
        self.assertEqual(error.text, "You can't have an empty sermon title")

        # She tries again with some text for the item, which now works
        self.browser.find_element_by_id('id_sermon_title').send_keys('First Sermon Title\n')
        self.check_for_row_in_list_table('First Sermon Title')

        # Perversely, she now decides to submit a second blank sermon title
        self.browser.find_element_by_id('add_sermon').click()
        self.browser.find_element_by_id('id_sermon_title').send_keys('\n')

        # She receives a similar warning on the list page
        error = self.browser.find_element_by_css_selector('.has-error')
        self.assertEqual(error.text, "You can't have an empty sermon title")

        # And she can correct it by filling some text in
        self.browser.find_element_by_id('id_sermon_title').send_keys('Sermon Title #2\n')
        self.check_for_row_in_list_table('First Sermon Title')
        self.check_for_row_in_list_table('Sermon Title #2')
