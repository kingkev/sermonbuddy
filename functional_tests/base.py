import sys
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver


class FunctionalTest(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        #If there's a liveserver arg, test against that server_url
        for arg in sys.argv:
            if 'liveserver' in arg: 
                cls.server_url = 'http://' + arg.split('=')[1]
                return 
        super().setUpClass()
        #If the arg is not found, use the ordinary live_server_url, 
        #but name it server_url
        cls.server_url = cls.live_server_url

    @classmethod
    def tearDownClass(cls):
        if cls.server_url == cls.live_server_url:
            super().tearDownClass()

    def setUp(self):
        self.browser = webdriver.PhantomJS()
        #Wait up tp 3 seconds for an element to appear
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])
