from django.conf.urls import patterns, url

urlpatterns = patterns('',
    # Examples:
    #url(r'^admin/', include(admin.site.urls)),
    url(r'^(\d+)/$', 'notes.views.view_notes', name='view_notes'),
    url(r'^(\d+)/add$', 'notes.views.add_sermon', name='add_sermon'),
    url(r'^new$', 'notes.views.new_sermon', name='new_sermon'),
)
