from django.db import models
from django.core.urlresolvers import reverse

# Create your models here.
class Series(models.Model):
	
	def get_absolute_url(self):
		return reverse('view_notes', args=[self.id])

class Sermon(models.Model):
    title = models.CharField(max_length=100)
    speaker = models.CharField(max_length=100, default='', null=True, blank=True)
    text = models.TextField(default='')
    date = models.DateTimeField(auto_now_add=True)
    series = models.ForeignKey(Series, default=None)

    class Meta:
    	ordering = ['-date']
