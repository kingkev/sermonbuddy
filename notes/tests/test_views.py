from django.core.urlresolvers import resolve
from django.template.loader import render_to_string
from django.test import TestCase
from django.http import HttpRequest
from django.utils.html import escape

from notes.views import home_page
from notes.models import Sermon, Series

# Create your tests here.
class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('notes/home.html')
        #Allows us to compare strings with strings, instead of bytes
        self.assertEqual(response.content.decode(), expected_html)

class NewSermonTest(TestCase):

    def test_saving_a_POST_request(self):
        self.client.post(
            '/notes/new',
            data={
            'title': 'A new sermon title',
            'speaker': 'The Sermon Speaker',
            'notes': 'Sermon Contents',
                }
        )

        self.assertEqual(Sermon.objects.count(), 1)

        new_item = Sermon.objects.first()

        self.assertEqual(new_item.title, 'A new sermon title')
        self.assertEqual(new_item.speaker, 'The Sermon Speaker')
        self.assertEqual(new_item.text, 'Sermon Contents')

    def test_redirects_after_POST(self):
        response = self.client.post(
            '/notes/new',
            data={
            'title': 'A new sermon title',
            'speaker': 'The Sermon Speaker',
            'notes': 'Sermon Contents',
                }
        )
        new_series = Series.objects.first() #Series created in new_sermon view
        self.assertRedirects(response, '/notes/%d/' % (new_series.id,))

    def test_can_save_a_POST_request_to_an_existing_series(self):
        other_series = Series.objects.create()
        correct_series = Series.objects.create()

        self.client.post(
            '/notes/%d/add' % (correct_series.id,),
            data={
            'title': 'A new sermon title',
            'speaker': 'The Sermon Speaker',
            'notes': 'Sample Sermon Contents',
                }
        )
        #The Sermon is saved to the database
        self.assertEqual(Sermon.objects.count(), 1)
        new_item = Sermon.objects.first()
        self.assertEqual(new_item.text, 'Sample Sermon Contents')
        self.assertEqual(new_item.series, correct_series)


    def test_redirects_to_list_view(self):
        other_series = Series.objects.create()
        correct_series = Series.objects.create()

        response = self.client.post(
            '/notes/%d/add' % (correct_series.id,),
            data={
            'title': 'A new sermon title',
            'speaker': 'The Sermon Speaker',
            'notes': 'Sample Sermon Contents',
                }
        )

        self.assertRedirects(response, '/notes/%d/' % (correct_series.id,))

    def test_validation_errors_are_sent_back_to_new_sermon_template(self):
        response = self.client.post('/notes/new', data={
            'title': '',
            'speaker': 'The Sermon Speaker',
            'notes': 'Sample Sermon Contents',}
            )

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'notes/new_sermon.html')
        expected_error = escape("You can't have an empty sermon title")
        self.assertContains(response, expected_error)

    def test_invalid_sermon_items_arent_saved(self):
        self.client.post('/notes/new', data={
            'title': '',
            'speaker': 'The Sermon Speaker',
            'notes': 'Sample Sermon Contents',}
            )
        self.assertEqual(Series.objects.count(), 0)
        self.assertEqual(Sermon.objects.count(), 0)

class ListViewTest(TestCase):

    def test_uses_list_template(self):
        series = Series.objects.create()
        response = self.client.get('/notes/%d/' % (series.id,))
        self.assertTemplateUsed(response, 'notes/list.html')

    def test_displays_only_items_for_that_series(self):
        correct_series = Series.objects.create()
        other_series = Series.objects.create()

        Sermon.objects.create(
            title='A new Sermon Title', 
            speaker='Me', 
            text='Something I wrote',
            series = correct_series,)

        Sermon.objects.create(
            title='Sermon on the Mountain', 
            speaker='Jesus', 
            text='Happy are the meek',
            series = other_series,)

        response = self.client.get('/notes/%d/' % (correct_series.id,))

        self.assertContains(response, 'A new Sermon Title')
        self.assertNotContains(response, 'Sermon on the Mountain')

    def test_passes_correct_sermon_series_to_template(self):
        correct_series = Series.objects.create()
        other_series = Series.objects.create()

        response = self.client.get('/notes/%d/' % (correct_series.id,))
        self.assertEqual(response.context['series'], correct_series)

    def test_retrieves_items_in_latest_first_order(self):
        saved_series = Series.objects.create()

        Sermon.objects.create(
            title='A new Sermon Title', 
            speaker='Me', 
            text='Something I wrote',
            series = saved_series,)

        Sermon.objects.create(
            title='Sermon on the Mountain', 
            speaker='Jesus', 
            text='Happy are the meek',
            series = saved_series,)

        saved_items = Sermon.objects.all()
        self.assertEqual(saved_items.count(), 2)

        retrieved_series = Series.objects.first()

        first_retrieved_item = saved_items[0]
        second_retrieved_item = saved_items[1]

        self.assertEqual(first_retrieved_item.title, 'Sermon on the Mountain')
        self.assertEqual(first_retrieved_item.text, 'Happy are the meek')
        self.assertEqual(first_retrieved_item.speaker, 'Jesus')
        self.assertEqual(first_retrieved_item.series, retrieved_series)

        self.assertEqual(second_retrieved_item.title, 'A new Sermon Title')
        self.assertEqual(second_retrieved_item.text, 'Something I wrote')
        self.assertEqual(second_retrieved_item.speaker, 'Me')
        self.assertEqual(second_retrieved_item.series, retrieved_series)

class AddSermonTest(TestCase):

    def test_validation_errors_are_sent_back_to_add_sermon_template(self):
        series = Series.objects.create()
        response = self.client.post('/notes/{0}/add'.format(series.id),
            data={
            'title': '',
            'speaker': 'The Sermon Speaker',
            'notes': 'Sample Sermon Contents',}
            )

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'notes/add_sermon.html')
        expected_error = escape("You can't have an empty sermon title")
        self.assertContains(response, expected_error)

    def test_invalid_sermon_items_arent_saved(self):
        series = Series.objects.create()
        self.client.post('/notes/{0}/add'.format(series.id),
            data={
            'title': '',
            'speaker': 'The Sermon Speaker',
            'notes': 'Sample Sermon Contents',}
            )
        self.assertEqual(Sermon.objects.count(), 0)
