from django.core.urlresolvers import resolve
from django.template.loader import render_to_string
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.http import HttpRequest

from notes.views import home_page
from notes.models import Sermon, Series

# Create your tests here.
class SeriesAndSermonModelTest(TestCase):

    def test_saving_and_retrieving_items(self):
        series = Series()
        series.save()

        first_item = Sermon()
        first_item.title = 'A sermon title'
        first_item.text = 'The first (ever) sermon note item'
        first_item.speaker = 'Speaker 1'
        first_item.series = series
        first_item.save()

        second_item = Sermon()
        second_item.title = 'Another Sermon title'
        second_item.text = 'The second sermon is this one'
        second_item.speaker = 'Speaker 2'
        second_item.series = series
        second_item.save()

        saved_series = Series.objects.first()
        self.assertEqual(series, saved_series)

        saved_items = Sermon.objects.all()
        self.assertEqual(saved_items.count(), 2)

        ##Items are returned in last first order
        first_saved_item = saved_items[1]
        second_saved_item = saved_items[0]
        
        self.assertEqual(first_saved_item.title, 'A sermon title')
        self.assertEqual(first_saved_item.text, 'The first (ever) sermon note item')
        self.assertEqual(first_saved_item.speaker, 'Speaker 1')
        self.assertEqual(first_saved_item.series, series)

        self.assertEqual(second_saved_item.title, 'Another Sermon title')
        self.assertEqual(second_saved_item.text, 'The second sermon is this one')
        self.assertEqual(second_saved_item.speaker, 'Speaker 2')
        self.assertEqual(second_saved_item.series, series)

    def test_cannot_save_empty_sermon_items(self):
        serie = Series.objects.create()
        sermon = Sermon(series=serie, title='')
        #Check that adding an empty sermon title raises a ValidationError
        with self.assertRaises(ValidationError):
            sermon.save()
            sermon.full_clean() #Run validation

    def test_get_absolute_url(self):
        series = Series.objects.create()
        self.assertEqual(series.get_absolute_url(), '/notes/%d/' % (series.id,))
