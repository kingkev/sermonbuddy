# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0003_auto_20140911_1207'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sermon',
            name='speaker',
            field=models.CharField(blank=True, null=True, default='', max_length=100),
        ),
    ]
