# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0005_series'),
    ]

    operations = [
        migrations.AddField(
            model_name='sermon',
            name='series',
            field=models.ForeignKey(default=None, to='notes.Series'),
            preserve_default=True,
        ),
    ]
