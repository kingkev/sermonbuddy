# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0002_sermon_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='sermon',
            name='date',
            field=models.DateField(default=datetime.date(2014, 9, 11), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='sermon',
            name='speaker',
            field=models.CharField(max_length=100, default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sermon',
            name='title',
            field=models.CharField(max_length=100, default='Pastor M'),
            preserve_default=False,
        ),
    ]
