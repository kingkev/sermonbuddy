# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0006_sermon_series'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sermon',
            options={'ordering': ['-date']},
        ),
        migrations.AlterField(
            model_name='sermon',
            name='date',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
