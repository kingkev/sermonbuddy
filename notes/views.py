from django.shortcuts import render, redirect
from django.core.exceptions import ValidationError
from notes.models import Sermon, Series

# Create your views here.
def home_page(request):
    return render(request, 'notes/home.html')

def new_sermon(request):
    if request.method == 'POST':
        series = Series.objects.create()
        sermon = Sermon(
            title=request.POST['title'], 
            speaker=request.POST['speaker'], 
            text=request.POST['notes'],
            series=series,
        )
        try:
            sermon.full_clean()
            sermon.save()
        except ValidationError:
            series.delete()
            error = "You can't have an empty sermon title"
            return render(request, 'notes/new_sermon.html', {'error': error, 'series': series})
        return redirect(series)
    return render(request, 'notes/new_sermon.html')

def view_notes(request, series_id):
    series = Series.objects.get(id=series_id)
    return render(request, 'notes/list.html', {'series': series})

def add_sermon(request, series_id):
    series = Series.objects.get(id=series_id)
    if request.method == 'POST':
        added_sermon = Sermon(
                title=request.POST['title'], 
                speaker=request.POST['speaker'], 
                text=request.POST['notes'],
                series=series,
            )
        try:
            added_sermon.full_clean()
            added_sermon.save()
        except ValidationError:
            error = "You can't have an empty sermon title"
            return render(request, 'notes/add_sermon.html', {'error': error, 'series': series})
        return redirect(series)
    return render(request, 'notes/add_sermon.html', {'series': series})
